from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from app import db

storytag = db.Table('storytag',
    db.Column('story_id', db.Integer, db.ForeignKey('story.story_id'), primary_key=True),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.tag_id'), primary_key=True)
)

resourcetag = db.Table('resourcetag',
    db.Column('resource_id', db.Integer, db.ForeignKey('resource.res_id'), primary_key=True),
    db.Column('tag_id', db.Integer, db.ForeignKey('tag.tag_id'), primary_key=True)
)

class Story(db.Model):
    __tablename__ = 'story'
    story_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=True)
    email = db.Column(db.String(50), nullable=True)
    content = db.Column(db.String(63206), nullable=False)
    lat = db.Column(db.Float(Precision=64), nullable=True)
    lng = db.Column(db.Float(Precision=64), nullable=True)

    user_id = db.Column(db.Integer, db.ForeignKey('user.email'), nullable=True)
    images = db.relationship('Image', backref='story', lazy=True, primaryjoin="Story.story_id == Image.story_id")
    videos = db.relationship('Video', backref='story', lazy=True, primaryjoin="Story.story_id == Video.story_id")
    tags = db.relationship('Tag', secondary=storytag, lazy='subquery', backref=db.backref('stories', lazy=True))

    def __init__(self, name, email, content, lat, lng, user_id):
        self.name = name
        self.email = email
        self.content = content
        self.lat = lat
        self.lng = lng
        self.user_id = user_id

class Resource(db.Model):
    __tablename__ = 'resource'
    res_id = db.Column( db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    link = db.Column(db.String(50))
    tags = db.relationship('Tag', secondary=resourcetag, lazy='subquery', backref=db.backref('resources', lazy=True))

    def __init__(self, name, link):
        self.name = res_name
        self.link = link

class Tag(db.Model):
    __tablename__ = 'tag'
    tag_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))

    def __init__(self, name, link):
        self.name = name
        self.link = link

class User(db.Model):
    __tablename__ = 'user'
    email = db.Column(db.String(100), primary_key=True)
    name = db.Column(db.String(100))
    comments = db.relationship('Comment', backref='user', lazy=True, primaryjoin="User.email == Comment.user_id")
    stories = db.relationship('Story', backref='user', lazy=True)

    def __init__(self, email, name):
        self.email = email
        self.name = name

class Comment(db.Model):
    __tablename__ = 'comment'
    com_id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(63206))
    user_id = db.Column(db.Integer, db.ForeignKey('user.email'))

    def __init__(self, content):
        self.content = content

class Image(db.Model):
    __tablename__ = 'image'
    img_id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(63206))
    story_id = db.Column(db.Integer, db.ForeignKey('story.story_id'))

    def __init__(self, content):
        self.content = content

class Video(db.Model):
    __tablename__ = 'video'
    vid_id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(63206))
    story_id = db.Column(db.Integer, db.ForeignKey('story.story_id'))

    def __init__(self, content):
        self.content = content

def clear(session):
    meta = db.metadata
    for table in reversed(meta.sorted_tables):
        print('Clear table %s' % table)
        session.execute(table.delete())
    session.commit()

# Create tables
db.create_all()