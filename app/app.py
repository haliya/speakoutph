from flask import Flask, render_template
from jinja2 import TemplateNotFound
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///haliyadb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

@app.route('/')
@app.route('/home/')
def home():
	try:
		return render_template('home.html')
	except TemplateNotFound:
		abort(404)

@app.route('/stories/')
def stories():
	try:
		return render_template('stories.html')
	except TemplateNotFound:
		abort(404)

@app.route('/data/')
def data():
	try:
		return render_template('data.html')
	except TemplateNotFound:
		abort(404)

@app.route('/resources/')
def resources():
	try:
		return render_template('resources.html')
	except TemplateNotFound:
		abort(404)